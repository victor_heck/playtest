<?php

class CompanyController extends \BaseController {
    const COMPANY_LIST = "company.company-list";
    const COMPANY_NEW = "company.company-new";
    const COMPANY_DETAIL = "company.company-detail";

    public function listCompanies() {
        if (!Auth::check()) {
            return Redirect::to("/", ["expired", 1]);
        }

        View::composer(CompanyController::COMPANY_LIST, function ($view) {
            $view->with("loggedUser", Auth::getUser());
            $view->with("menu", "company");
            $view->with("companies", Company::all());
        });


        return View::make(CompanyController::COMPANY_LIST);
    }

    public function newCompany() {
        if (!Auth::check()) {
            return Redirect::to("/", ["expired", 1]);
        }
        View::composer(CompanyController::COMPANY_NEW, function ($view) {
            $view->with("loggedUser", Auth::getUser());
            $view->with("menu", "company");
            $view->with("states", State::all());
        });

        return View::make(CompanyController::COMPANY_NEW);
    }

    public function createCompany() {
        if (!Auth::check()) {
            return Response::make("",401);
        }

        if (!Input::has("name") || !Input::has("cnpj") || !Input::has("state")) {
            return Response::make("invalid", 400);
        }

        if (Company::where("cnpj", "=", Input::get("cnpj"))->exists()) {
            return Response::make("exists", 400);
        }

        Company::create([
            "company_name" => Input::get("name"),
            "cnpj" => Input::get("cnpj"),
            "uf_id" => Input::get("state")
        ]);

        return Response::make("", 204);
    }

    public function detailCompany($id) {
        if (!Auth::check()) {
            return Redirect::to("/", ["expired", 1]);
        }
        View::composer(CompanyController::COMPANY_DETAIL, function ($view) {
            $view->with("loggedUser", Auth::getUser());
            $view->with("menu", "company");
        });

        return View::make(CompanyController::COMPANY_DETAIL, [
            "company" => Company::find($id)
        ]);
    }
}
