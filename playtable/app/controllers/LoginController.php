<?php

class LoginController extends BaseController {

    public function authenticate() {
        $user = Input::get("user", "");
        $pass = Input::get("password", "");

        $auth = Auth::attempt(array("user" => $user, "password" => $pass));
        Log::info($auth);
        if ($auth) {
            return Redirect::intended("main");
        } else {
            return Redirect::to("/?invalid=true");
        }
    }

    public function logout(){
        Auth::logout();
        return Redirect::to("/");
    }


}
