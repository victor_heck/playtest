<?php

class UserController extends BaseController {
    const USER_LIST = "user.user-list";
    const USER_NEW = "user.user-new";

    public function listUsers() {
        if (!Auth::check()) {
            return Redirect::to("/", ["expired", 1]);
        }
        View::composer(UserController::USER_LIST, function ($view) {
            $view->with("loggedUser", Auth::getUser());
            $view->with("menu", "user");
            $view->with("users", User::all());
        });

        return View::make(UserController::USER_LIST);
    }

    public function newUser() {
        if (!Auth::check()) {
            return Redirect::to("/", ["expired", 1]);
        }
        View::composer(UserController::USER_NEW, function ($view) {
            $view->with("loggedUser", Auth::getUser());
            $view->with("menu", "user");
        });

        return View::make(UserController::USER_NEW);
    }

    public function createUser() {
        if (!Auth::check()) {
            return Redirect::to("/", ["expired", 1]);
        }

        if (!Input::has("name") || !Input::has("user") || !Input::has("pass")) {
            return Redirect::to("user/new?exists=1");
        }

        if(User::where("name", "=", Input::get("user"))->exists()){
            return Redirect::to("user/new?exists=1");
        }

        User::create([
            "name" => Input::get("name"),
            "user" => Input::get("user"),
            "password" => Hash::make(Input::get("pass"))]);

        return Redirect::to("user/list");
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id) {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id) {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return Response
     */
    public function update($id) {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id) {
        //
    }


}
