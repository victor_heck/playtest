<?php

class MainController extends BaseController {

    const MAIN_PAGE = "mainpage";

    public function mainPage() {
        if (!Auth::check()) {
            return Redirect::to("/");
        }
        View::composer(MainController::MAIN_PAGE, function ($view) {
            $view->with("loggedUser", Auth::getUser());
            $view->with("menu", "");
        });
        return View::make(MainController::MAIN_PAGE);
    }
}
