<?php

class PlayTableController extends \BaseController {
    const PLAYTABLE_NEW = "playtable.playtable-new";
    const PLAYTABLE_LIST = "playtable.playtable-list";

    public function newPlayTable($id = 0) {
        if (!Auth::check()) {
            return Redirect::to("/?expired=1");
        }
        View::composer(self::PLAYTABLE_NEW, function ($view) {
            $view->with("loggedUser", Auth::getUser());
            $view->with("companies", Company::all());
        });

        return View::make(self::PLAYTABLE_NEW, [
            "companyId" => $id,
            "menu" => $id == 0 ? "" : "company"
        ]);
    }

    public function createPlayTable() {
        if (!Auth::check()) {
            return Response::make("", 401);
        }

        if (!Input::has("company") || !Input::has("serial") || !Input::has("color") || !Input::has("buy_date")) {
            return Response::make("invalid", 400);
        }

        if (PlayTable::where("serial", "=", Input::get("serial"))->exists()) {
            return Response::make("exists", 400);
        }

        PlayTable::create([
            "serial" => Input::get("serial"),
            "company_id" => Input::get("company"),
            "color" => Input::get("color"),
            "buy_date" => Input::get("buy_date")
        ]);

        return Response::make("", 204);
    }

    public function listPlayTables() {
        if (!Auth::check()) {
            return Redirect::to("/?expired=1");
        }
        View::composer(self::PLAYTABLE_LIST, function ($view) {
            $view->with("loggedUser", Auth::getUser());
            $view->with("menu", "playtable");
        });
        return View::make(self::PLAYTABLE_LIST);
    }

    public function listPlaytablesRest() {
        if (!Auth::check()) {
            return Response::make("", 400);
        }
        return Response::json(PlayTable::with("Company")->get());
    }

    public function deletePlayTable(){
        if (!Auth::check()) {
            return Response::make("", 401);
        }
        if (!Input::has("id")) {
            return Response::make("invalid", 400);
        }
        PlayTable::destroy(Input::get("id"));
        return Response::make("",204);
    }
}
