<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showWelcome');

Route::get('/logout', 'LoginController@logout');

Route::post("/login", "LoginController@authenticate");

Route::get("/main","MainController@mainPage");

Route::get("/user/list", "UserController@listUsers");

Route::get("/user/new", "UserController@newUser");

Route::post("/user/createUser", "UserController@createUser");

Route::get("/company/list", "CompanyController@listCompanies");

Route::get("/company/new", "CompanyController@newCompany");

Route::get("/company/detail/{id}", "CompanyController@detailCompany");

Route::post("/company/createCompany", "CompanyController@createCompany");

Route::get("/playtable/list", "PlayTableController@listPlayTables");

Route::get("/playtable/new/{id?}", "PlayTableController@newPlayTable");

Route::post("/playtable/createPlayTable", "PlayTableController@createPlayTable");

Route::get("/playtable/listPlaytablesRest", "PlayTableController@listPlaytablesRest");

Route::delete("/playtable/deletePlayTable", "PlayTableController@deletePlayTable");
