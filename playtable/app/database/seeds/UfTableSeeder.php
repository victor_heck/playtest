<?php

class UfTableSeeder extends Seeder {
    public function run() {
        $count = State::count();
        if(!$count) {
            State::create(array("name" => "Acre", "acronym" => "AC"));
            State::create(array("name" => "Alagoas", "acronym" => "AL"));
            State::create(array("name" => "Amapá", "acronym" => "AP"));
            State::create(array("name" => "Amazonas", "acronym" => "AM"));
            State::create(array("name" => "Bahia", "acronym" => "BA"));
            State::create(array("name" => "Ceará", "acronym" => "CE"));
            State::create(array("name" => "Distrito Federal", "acronym" => "DF"));
            State::create(array("name" => "Espírito Santo", "acronym" => "ES"));
            State::create(array("name" => "Goiás", "acronym" => "GO"));
            State::create(array("name" => "Maranhão", "acronym" => "MA"));
            State::create(array("name" => "Mato Grosso", "acronym" => "MT"));
            State::create(array("name" => "Mato Grosso do Sul", "acronym" => "MS"));
            State::create(array("name" => "Minas Gerais", "acronym" => "MG"));
            State::create(array("name" => "Pará", "acronym" => "PA"));
            State::create(array("name" => "Paraíba", "acronym" => "PB"));
            State::create(array("name" => "Paraná", "acronym" => "PR"));
            State::create(array("name" => "Pernambuco", "acronym" => "PE"));
            State::create(array("name" => "Piauí", "acronym" => "PI"));
            State::create(array("name" => "Rio de Janeiro", "acronym" => "RJ"));
            State::create(array("name" => "Rio Grande do Norte", "acronym" => "RN"));
            State::create(array("name" => "Rio Grande do Sul", "acronym" => "RS"));
            State::create(array("name" => "Rondônia", "acronym" => "RO"));
            State::create(array("name" => "Roraima", "acronym" => "RR"));
            State::create(array("name" => "Santa Catarina", "acronym" => "SC"));
            State::create(array("name" => "São Paulo", "acronym" => "SP"));
            State::create(array("name" => "Sergipe", "acronym" => "SE"));
            State::create(array("name" => "Tocantins", "acronym" => "TO"));
        }
    }
}
