<?php


class UserTableSeeder extends Seeder{

    public function run() {
        $count = User::count();

        if($count == 0){
            User::create(array(
                "name" => "Admin",
                "user" => "admin",
                "password" => Hash::make("pass")
            ));
        }
    }
}
