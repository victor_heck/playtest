<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("company", function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedInteger("uf_id");
            $table->string("company_name");
            $table->string("cnpj");
            $table->timestamps();
            $table->foreign("uf_id")->references("id")->on("uf");
        });

        Schema::create("playtable", function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedInteger("company_id");
            $table->string("serial");
            $table->string("color");
            $table->date("buy_date");
            $table->timestamps();
            $table->foreign("company_id")->references("id")->on("company");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop("playtable");
        Schema::drop("company");
    }
}
