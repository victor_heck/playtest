@extends("../base.bootstrap-base")
@section("styles")
    <link rel="stylesheet" href="{{ URL::asset("styles/menu.css") }}">
    <link rel="stylesheet" href="{{ URL::asset("styles/list.css") }}">
@stop
@section("container")
    @include("../menu/menu")
    <div class="main-container col-10">
        <h1 class="list-title">Nova empresa</h1>

        <div class="alert alert-danger" role="alert" id="exists" style="display: none;">
            Já existe uma empresa com esse CNPJ.
        </div>
        <div class="alert alert-danger" role="alert" id="invalid" style="display: none;">
            Todos os campos são obrigatórios.
        </div>
        <form>
            <div class="form-group">
                <label for="name">CNPJ: </label>
                <input class="form-control" type="text" name="cnpj" id="cnpj"/>
            </div>
            <div class="form-group">
                <label for="name">Nome fantasia: </label>
                <input class="form-control" type="text" name="name" id="name"/>
            </div>
            <div class="form-group">
                <label for="state">Estado: </label>
                <select class="form-control" name="state" id="state">
                    @foreach($states as $state)
                        <option value="{{$state->id}}">{{$state->acronym}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" id="submit-button" class="btn btn-success">Enviar</button>
        </form>
    </div>


@stop

@section("scripts")
    <script src="{{asset("packages\jquery.mask.min.js")}}"></script>
    <script>
        $(document).ready(function () {
            $("#cnpj").mask("00.000.000/0000-00");
            $("#submit-button").on("click", (e) => {
                e.preventDefault();
                const cnpjInput = $("#cnpj");
                const nameInput = $("#name");

                const validCnpj = cnpjInput.val().length === 18;
                const validName = nameInput.val().length > 0;
                if (!validCnpj || !validName) {
                    $("#invalid").show();
                    return;
                }
                $.ajax("/company/createCompany", {
                    method: "POST",
                    data: {
                        "name": nameInput.val(),
                        "cnpj": cnpjInput.val(),
                        "state": $("#state").val()
                    }
                }).done((data) => {
                    window.location.href = "/company/list?success=true"
                }).fail((jqxhr) => {
                    $(".alert").hide();
                    $("#" + jqxhr.responseText).show();
                });
            })
        })
    </script>
@stop
