@extends("../base.bootstrap-base")
@section("styles")
    <link rel="stylesheet" href="{{ URL::asset("styles/menu.css") }}">
    <link rel="stylesheet" href="{{ URL::asset("styles/list.css") }}">
@stop
@section("container")
    @include("../menu/menu")
    <div class="main-container col-10">
        <h1 class="list-title">Listagem de empresas</h1>
        <button class="list-new btn btn-success" onclick="goToNew()">Novo</button>
        <table class="col-12 table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">CNPJ</th>
                <th scope="col">Nome Fantasia</th>
                <th scope="col">Estado</th>
                <th scope="col">Numero de PlayTables</th>
                <th scope="col">Criado em</th>
                <th scope="col">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <th scope="row">{{ $company->id }}</th>
                    <td>{{ $company->cnpj}}</td>
                    <td>{{ $company->company_name }}</td>
                    <td>{{ $company->state->acronym }}</td>
                    <td>{{ $company->playtables->count() }}</td>
                    <td>{{ $company->created_at->format("d/m/Y") }}</td>
                    <td>
                        <button id="detalhar" class="btn btn-primary btn-sm" onclick="goToDetail({{ $company->id }})">Detalhar</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        function goToNew() {
            window.location.href = '/company/new';
        }

        function goToDetail(id) {
            window.location.href = '/company/detail/' + id;
        }
    </script>
@stop
