@extends("../base.bootstrap-base")
@section("styles")
    <link rel="stylesheet" href="{{ URL::asset("styles/menu.css") }}">
    <link rel="stylesheet" href="{{ URL::asset("styles/list.css") }}">
@stop
@section("container")
    @include("../menu/menu")
    <div class="main-container col-10">
        <h1 class="list-title">{{$company->company_name}}</h1>
        <div class="details">
            <div>
                CNPJ: {{$company->cnpj}}
            </div>
            <div> UF: {{$company->state->name}}</div>
        </div>

        <div class="col-12 list-container">
            <h2 class="list-title">Playtables</h2>
            <button class="list-new btn btn-success" onclick="goToNew({{ $company->id }})">Novo</button>
            <table class="col-12 table">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Número de série</th>
                    <th scope="col">Cor</th>
                    <th scope="col">Data de compra</th>
                </tr>
                </thead>
                <tbody>
                @foreach($company->playtables as $playtable)
                    <tr>
                        <th scope="row">{{ $playtable->id }}</th>
                        <td>{{ $playtable->serial}}</td>
                        <td>{{ $playtable->color}}</td>
                        <td>{{ $playtable->buy_date->format("d/m/Y")}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@stop

@section("scripts")
    <script src="{{asset("packages\jquery.mask.min.js")}}"></script>
    <script>
        function goToNew(id) {
            window.location.href = '/playtable/new/'+id;
        }
    </script>
@stop
