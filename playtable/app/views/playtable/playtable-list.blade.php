@extends("../base.bootstrap-base")
@section("styles")
    <link rel="stylesheet" href="{{ URL::asset("styles/menu.css") }}">
    <link rel="stylesheet" href="{{ URL::asset("styles/list.css") }}">
@stop
@section("container")
    @include("../menu/menu")
    <div class="main-container col-10">
        <h1 class="list-title">Listagem de empresas</h1>
        <button class="list-new btn btn-success" onclick="goToNew()">Novo</button>
        <div class="alert alert-danger" role="alert" id="deleteMessage" style="display: none;">
            Playtable removida
        </div>
        <table class="col-12 table" id="table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Numero de série</th>
                <th scope="col">Empresa</th>
                <th scope="col">Cor</th>
                <th scope="col">Data de compra</th>
                <th scope="col">Ações</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@stop

@section("scripts")
    <script src="{{asset("packages\moment.min.js")}}"></script>
    <script>
        function goToNew() {
            window.location.href = '/playtable/new';
        }

        function deletePlayTable(id) {
            $.ajax("/playtable/deletePlayTable", {
                method: "DELETE",
                data: {
                    "id": id
                }
            }).done((data) => {
                $("#deleteMessage").show();
                loadPlayTables();
            }).fail((jqxhr) => {
                alert("Ocorreu um erro");
            });
        }

        function loadPlayTables() {
            $.ajax("/playtable/listPlaytablesRest", {
                method: "GET",
            }).done((data) => {
                const tableBody = $("#table").children("tbody");
                tableBody.empty();
                data.forEach((row) => {
                    const date = moment(row.buy_date);
                    tableBody.append(
                        "<tr>" +
                        "   <th scope=\"row\">" + row.id + "</th>" +
                        "       <td>" + row.serial + "</td>" +
                        "       <td>" + row.company.company_name + "</td>" +
                        "       <td>" + row.color + "</td>" +
                        "       <td>" + date.format("L") + "</td>" +
                        "       <td>" +
                        "           <button id=\"detalhar\" class=\"btn btn-primary btn-sm\" onclick=\"deletePlayTable(" + row.id + ")\">" +
                        "               Excluir" +
                        "           </button>" +
                        "       </td>" +
                        "</tr>");
                });
            }).fail((jqxhr) => {
                alert("Ocorreu um erro");
            });
        }

        $(document).ready(function () {
            loadPlayTables();
        });
    </script>
@stop
