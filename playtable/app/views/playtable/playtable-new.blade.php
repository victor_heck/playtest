@extends("../base.bootstrap-base")
@section("styles")
    <link rel="stylesheet" href="{{ URL::asset("styles/menu.css") }}">
    <link rel="stylesheet" href="{{ URL::asset("styles/list.css") }}">
@stop
@section("container")
    @include("../menu/menu")
    <div class="main-container col-10">
        <h1 class="list-title">Nova PlayTable</h1>
        <div class="alert alert-danger" role="alert" id="exists" style="display: none;">
            Já existe uma PlayTable com esse numero de série.
        </div>
        <div class="alert alert-danger" role="alert" id="invalid" style="display: none;">
            Todos os campos são obrigatórios.
        </div>
        <form action="/user/createUser" method="post">
            <div class="form-group">
                <label for="company">Empresa: </label>
                <select class="form-control" name="company" id="company">
                    @foreach($companies as $company)
                        @if($companyId != 0 && $company->id == $companyId)
                            <option value="{{$company->id}}" selected>{{$company->company_name}}</option>
                        @else
                            <option value="{{$company->id}}">{{$company->company_name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="serial">Numero de série: </label>
                <input class="form-control" type="text" name="serial" id="serial"/>
            </div>
            <div class="form-group">
                <label for="color">Cor: </label>
                <input class="form-control" type="text" name="color" id="color"/>
            </div>
            <div class="form-group">
                <label for="buydate">Data de compra: </label>
                <input class="form-control" type="date" name="buydate" id="buydate"/>
            </div>
            <button type="submit" id="submit-button" class="btn btn-success">Enviar</button>
        </form>
    </div>
@stop

@section("scripts")
    <script>
        $(document).ready(function () {
            $("#submit-button").on("click", (e) => {
                e.preventDefault();

                const companyInput = $("#company");
                const serialInput = $("#serial");
                const colorInput = $("#color");
                const dateInput = $("#buydate");

                const validSerial = serialInput.val().length > 0;
                const validColor = colorInput.val().length > 0;
                const validDate = dateInput.val().length > 0;
                if (!validSerial ||
                    !validColor ||
                    !validDate) {
                    $("#invalid").show();
                    return;
                }
                $.ajax("/playtable/createPlayTable", {
                    method: "POST",
                    data: {
                        "company": companyInput.val(),
                        "serial": serialInput.val(),
                        "color": colorInput.val(),
                        "buy_date": dateInput.val()
                    }
                }).done((data) => {
                    const id = {{ $companyId  }};
                    if(id){
                        window.location.href = "/company/detail/" + companyInput.val();
                    }else{
                        window.location.href = "/playtable/list/";
                    }

                }).fail((jqxhr) => {
                    $(".alert").hide();
                    $("#" + jqxhr.responseText).show();
                });
            })
        })
    </script>
@stop
