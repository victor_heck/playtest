@extends("../base.bootstrap-base")
@section("styles")
    <link rel="stylesheet" href="{{ URL::asset("styles/menu.css") }}">
    <link rel="stylesheet" href="{{ URL::asset("styles/list.css") }}">
@stop
@section("container")
    @include("../menu/menu")
    <div class="main-container col-10">

        <h1 class="list-title">Listagem de usuários</h1>

        <button class="list-new btn btn-success" onclick="goToNew()">Novo</button>
        <table class="col-12 table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Nome</th>
                <th scope="col">Usuário</th>
                <th scope="col">Criado em</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->user }}</td>
                    <td>{{ $user->created_at->format("d/m/Y") }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <script>
        function goToNew() {
            window.location.href = '/user/new';
        }
    </script>
@stop
