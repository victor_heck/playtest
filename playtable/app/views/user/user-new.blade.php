@extends("../base.bootstrap-base")
@section("styles")
    <link rel="stylesheet" href="{{ URL::asset("styles/menu.css") }}">
    <link rel="stylesheet" href="{{ URL::asset("styles/list.css") }}">
@stop
@section("container")
    @include("../menu/menu")
    <div class="main-container col-10">
        <h1 class="list-title">Novo usuário</h1>
        @if(Input::has("exists"))
            <div class="alert alert-danger" role="alert">
                Já existe um usuário com esse usuário.
            </div>
        @elseif(Input::has("invalid"))
            <div class="alert alert-danger" role="alert">
                Todos os campos são obrigatórios.
            </div>
        @endif
        <form action="/user/createUser" method="post">
            <div class="form-group">
                <label for="name">Nome: </label>
                <input class="form-control" type="text" name="name" id="name"/>
            </div>
            <div class="form-group">
                <label for="user">Usuário: </label>
                <input class="form-control" type="text" name="user" id="user"/>
            </div>
            <div class="form-group">
                <label for="pass">Senha: </label>
                <input class="form-control" type="password" name="pass" id="pass"/>
            </div>
            <button type="submit" class="btn btn-success">Enviar</button>
        </form>
    </div>
@stop
