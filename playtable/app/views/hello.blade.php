@extends("base/bootstrap-base")

@section("container")
    <div class="row justify-content-md-center login-box">
        <div class="col-md-auto">
            <div style="text-align: center; margin-bottom: 15px">
                <img src="{{asset("img/logo.png")}}"/>
            </div>
            <form action="login" method="post">
                <div class="form-group">
                    <label for="user">Usuário</label>
                    <input type="text" id="user" name="user" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="pass">Senha</label>
                    <input type="password" name="password" id="pass" class="form-control"/>
                </div>
                <div style="text-align: right;">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
    </div>
@stop
