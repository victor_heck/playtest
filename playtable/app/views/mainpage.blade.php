@extends("base/bootstrap-base")
@section("styles")
    <link rel="stylesheet" href="{{ URL::asset("styles/menu.css") }}">
@stop
@section("container")
    @include("menu/menu")
@stop
