<div class="menu-container col-2">
    <div class="menu-profile">
        <div class="menu-welcome">
            Bem vindo <span class="username">{{ $loggedUser->name }}</span>!
        </div>
        <div class="menu-logout">
            <a href="/logout">Sair</a>
        </div>
    </div>

    <div class="menu-item {{ $menu == "user" ? "selected":"" }}"><a href="/user/list">Usuários</a></div>
    <div class="menu-item {{ $menu == "company" ? "selected":"" }}"><a href="/company/list">Empresas</a></div>
    <div class="menu-item {{ $menu == "playtable" ? "selected":"" }}"><a href="/playtable/list">PlayTables</a></div>
</div>
