<?php


class State extends Eloquent {

    protected $table = 'uf';

    protected $fillable = ['name', 'acronym'];
}
