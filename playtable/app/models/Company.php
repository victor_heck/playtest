<?php


class Company extends Eloquent{

    protected $table = 'company';

    protected $fillable = ['company_name', 'cnpj', "uf_id"];

    public function playtables(){
        return $this->hasMany("PlayTable");
    }

    public function state(){
        return $this->belongsTo("State","uf_id");
    }

}
