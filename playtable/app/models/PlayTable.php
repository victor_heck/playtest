<?php


class PlayTable extends Eloquent{

    protected $table = "playtable";

    protected $fillable = ["company_id" ,'serial', "color", "buy_date"];

    protected $dates = [
        "buy_date"
    ];

    public function company(){
        return $this->belongsTo("Company");
    }

}
